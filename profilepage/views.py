from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'profilepage/Halaman-utama.html')

def biodata(request):
    return render(request, 'profilepage/Halaman-biodata.html')